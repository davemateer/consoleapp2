Install-WindowsFeature -Name web-server, Web-App-Dev,Web-Net-Ext, Web-Net-Ext45,Web-AppInit,Web-ASP,Web-Asp-Net,Web-Asp-Net45,Web-ISAPI-Ext,Web-ISAPI-Filter,Web-Includes,Web-WebSockets -IncludeManagementTools 

# Folders for websites
New-Item -ItemType Directory c:\inetpub\pathway2
New-Item -ItemType Directory c:\inetpub\pathway2.cdn

# Create new websites
New-WebSite -Name Pathway -Port 80 -HostHeader pathway360.hmsoftware.org -PhysicalPath "$env:systemdrive\inetpub\pathway2"
New-WebSite -Name Pathway_CDN -Port 80 -HostHeader cdn.willowpathway.hmsoftware.org -PhysicalPath "$env:systemdrive\inetpub\pathway2.cdn"

# Get chocolately
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

# GIT
choco install git.install -y

# SSMS
choco install sql-server-management-studio -y

# IIS Url Rewrite
# https://www.iis.net/downloads/microsoft/url-rewrite
# https://www.odity.co.uk/articles/how-to-redirect-http-to-https-with-iis-rewrite-module/
choco install urlrewrite -y

# Application Request Routing
# https://www.iis.net/downloads/microsoft/application-request-routing
# https://chocolatey.org/packages/iis-arr
choco install iis-arr -y

# Disable IE Enhanced Security Configuration
function Disable-InternetExplorerESC {
    $AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
    $UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
    Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0
    Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 0
    Stop-Process -Name Explorer
}
Disable-InternetExplorerESC


# SQL Server not working for a standalone install
# but this pulls the source ready to go

# Clone repo which contains SQL Server 2017 Media and config file
New-Item -ItemType Directory c:\sqlserver
cd c:\sqlserver
& 'C:/Program Files/Git/cmd/git.exe' clone https://davemateer@bitbucket.org/davemateer/consoleapp2.git .

# SQL Server Install
$pathToConfigurationFile = "c:\sqlserver\ConfigurationFile.ini"
$copyFileLocation = "C:\Temp\ConfigurationFile.ini"
New-Item "C:\Temp" -ItemType "Directory" -Force 
Copy-Item $pathToConfigurationFile $copyFileLocation -Force

## Run the command on first logon (trick to get SQL Server to install properly) 
function OnStartup-ScheduleSqlServerSetupFile {
     $regPath = "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce"
     $command = 'c:\sqlserver\SQLServer\Setup.exe /ConfigurationFile=c:/temp/ConfigurationFile.ini'
     New-ItemProperty $regPath -Name "SqlSetupCompletion" -Value $command | Out-Null
     Write-Host "Scheduled Execution of SQL Server"
}

OnStartup-ScheduleSqlServerSetupFile


# Download music app
# Invoke-WebRequest  https://github.com/Microsoft/dotnet-core-sample-templates/raw/master/dotnet-core-music-windows/music-app/music-store-azure-demo-pub.zip -OutFile c:\temp\musicstore.zip
# Expand-Archive C:\temp\musicstore.zip c:\music

# # Configure iis
# Remove-WebSite -Name "Default Web Site"
# Set-ItemProperty IIS:\AppPools\DefaultAppPool\ managedRuntimeVersion ""
# New-Website -Name "MusicStore" -Port 80 -PhysicalPath C:\music\ -ApplicationPool DefaultAppPool
# & iisreset

